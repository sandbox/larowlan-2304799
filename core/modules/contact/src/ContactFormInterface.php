<?php

/**
 * @file
 * Contains \Drupal\contact\Entity\ContactFormInterface.
 */

namespace Drupal\contact;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\Entity\ThirdPartySettingsInterface;
use Drupal\Core\Url;

/**
 * Provides an interface defining a contact form entity.
 */
interface ContactFormInterface extends ConfigEntityInterface, ThirdPartySettingsInterface {

  /**
   * Returns the message to be displayed to user.
   *
   * @return string
   *   A user message.
   */
  public function getMessage();

  /**
   * Returns list of recipient e-mail addresses.
   *
   * @return array
   *   List of recipient e-mail addresses.
   */
  public function getRecipients();

  /**
   * Returns an auto-reply message to send to the message author.
   *
   * @return string
   *   An auto-reply message.
   */
  public function getReply();

  /**
   * Returns the route for redirect.
   *
   * @return \Drupal\core\Url
   *   The redirect URL
   */
  public function getRedirectUrl();

  /**
   * Returns the weight of this category (used for sorting).
   *
   * @return int
   *   The weight of this category.
   */
  public function getWeight();

  /**
   * Sets the message to be displayed to the user.
   *
   * @param string $message
   *   The message to display after form is submitted.
   *
   * @return $this
   */
  public function setMessage($message);

  /**
   * Sets list of recipient e-mail addresses.
   *
   * @param array $recipients
   *   The desired list of e-mail addresses of this category.
   *
   * @return $this
   */
  public function setRecipients($recipients);

  /**
   * Sets an auto-reply message to send to the message author.
   *
   * @param string $reply
   *   The desired reply.
   *
   * @return $this
   */
  public function setReply($reply);

  /**
   * Sets the redirect route.
   *
   * @param \Drupal\core\Url $url
   *   The desired route.
   *
   * @return $this
   */
  public function setRedirectUrl(Url $url);

  /**
   * Sets the weight.
   *
   * @param int $weight
   *   The desired weight.
   *
   * @return $this
   */
  public function setWeight($weight);

}
