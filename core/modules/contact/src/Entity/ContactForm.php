<?php

/**
 * @file
 * Contains \Drupal\contact\Entity\ContactForm.
 */

namespace Drupal\contact\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\contact\ContactFormInterface;
use Drupal\Core\Entity\EntityStorageControllerInterface;
use Drupal\contact\CategoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Config\Entity\ThirdPartySettingsTrait;

/**
 * Defines the contact form entity.
 *
 * @ConfigEntityType(
 *   id = "contact_form",
 *   label = @Translation("Contact form"),
 *   handlers = {
 *     "access" = "Drupal\contact\ContactFormAccessControlHandler",
 *     "list_builder" = "Drupal\contact\ContactFormListBuilder",
 *     "form" = {
 *       "add" = "Drupal\contact\ContactFormEditForm",
 *       "edit" = "Drupal\contact\ContactFormEditForm",
 *       "delete" = "Drupal\contact\Form\ContactFormDeleteForm"
 *     }
 *   },
 *   config_prefix = "form",
 *   admin_permission = "administer contact forms",
 *   bundle_of = "contact_message",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "delete-form" = "entity.contact_form.delete_form",
 *     "edit-form" = "entity.contact_form.edit_form"
 *   }
 * )
 */
class ContactForm extends ConfigEntityBundleBase implements ContactFormInterface {

  use ThirdPartySettingsTrait;

  /**
   * The form ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable label of the category.
   *
   * @var string
   */
  protected $label;

  /**
   * The message displayed to user.
   *
   * @var string
   */
  protected $message;

  /**
   * List of recipient email addresses.
   *
   * @var array
   */
  protected $recipients = array();

  /**
   * The URL to redirect to.
   *
   * @var \Drupal\core\Url
   */
  protected $url;

  /**
   * An auto-reply message.
   *
   * @var string
   */
  protected $reply = '';

  /**
   * The weight of the category.
   *
   * @var int
   */
  protected $weight = 0;

  public function __construct(array $values, $entity_type) {
    parent::__construct($values, $entity_type);

    if (!empty($values['route']['route_name'])) {
      $this->url = new Url($values['route']['route_name']);
      if (!empty($values['route']['route_parameters'])) {
        $this->url->setRouteParameters($values['route']['route_parameters']);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getMessage() {
    return $this->get('message');
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipients() {
    return $this->recipients;
  }

  /**
   * {@inheritdoc}
   */
  public function setRecipients($recipients) {
    $this->recipients = $recipients;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getReply() {
    return $this->reply;
  }

  /**
   * {@inheritdoc}
   */
  public function getRedirectUrl() {
    return $this->url;
  }

  /**
   * {@inheritdoc}
   */
  public function setMessage($message) {
    $this->set('message', $message);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setReply($reply) {
    $this->reply = $reply;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setRedirectUrl(Url $url) {
    $this->url = $url;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function setWeight($weight) {
    $this->weight = $weight;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function toArray() {
    $properties = parent::toArray();
    if ($this->url) {
      $properties['route'] = [
        'route_name' => $this->url->getRouteName(),
        'route_parameters' => $this->url->getRouteParameters(),
      ];
    }
    else {
      $properties['route'] = [
        'route_name' => '<front>',
        'route_parameters' => [],
      ];
    }
    return $properties;
  }

}
